package com.oracle.exercise.nutsboltsmatch.model;

import java.io.Serializable;

/**
 * Nut java bean class
 * 
 * @author David Gonzalez
 * 
 */
public class Nut implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nut cconstructor
	 * @param size - The size of the nut
	 */
	public Nut(Integer size) {
		super();
		this.size = size;
	}

	/** Size of the bolt **/
	private Integer size;

	/**
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(Integer size) {
		this.size = size;
	}
	
	
	
	
}