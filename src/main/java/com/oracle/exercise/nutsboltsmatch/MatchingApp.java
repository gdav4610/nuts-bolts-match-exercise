package com.oracle.exercise.nutsboltsmatch;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.collections.api.bimap.MutableBiMap;
import org.eclipse.collections.impl.bimap.mutable.HashBiMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oracle.exercise.nutsboltsmatch.model.Bolt;
import com.oracle.exercise.nutsboltsmatch.model.Nut;

/**
 * 
 * The matching application
 *
 * @author David Gonzalez
 *
 */
public class MatchingApp {

	/**
	 * The logger of the class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MatchingApp.class);
	  
	/**
	 * Main method with application logic
	 * @param args Program arguments array
	 */
    public static void main( String[] args ){

    	// 1. We create the input Sets of nuts and bolts
		Set<Nut> nuts = fillInputNutsSet();
		Set<Bolt> bolts = fillInputBoltsSet();
		
		// 2. We create a temporary bolts map with each bolt size as key
		Map<Integer, Bolt> tempBoltsMap = createTemporaryBoltsMap(bolts);
		
		// 3. We create the resulting match map of nuts with bolts.
		// Although we are using Sets with unique items for the input, this kind of map helps us to make sure we have only unique keys, and unique values,
		// keeping a one-to-one relationship constraint between nuts and bolts. 
		MutableBiMap<Nut, Bolt> resultMatchMap = createResultMatchMap(nuts, tempBoltsMap);


		// 4. We just iterate over the resulting map
		iterateResultingMap(resultMatchMap);
		
    }




	/**
     * This method fills the input Sets of nuts with no defined order
     * 
     * @return  The set of nuts
     */
	private static Set<Nut> fillInputNutsSet() {

		Set<Nut> nuts = new HashSet<>();
		nuts.add(new Nut(1));
		nuts.add(new Nut(3));
		nuts.add(new Nut(5));
		nuts.add(new Nut(8));
		
		return nuts;
	}



	/**
     * This method fills the input Sets of bolts with no defined order
     * 
     * @return  The set of bolts
     */
	private static Set<Bolt> fillInputBoltsSet() {

		Set<Bolt> bolts = new HashSet<>();
		bolts.add(new Bolt(5));
		bolts.add(new Bolt(1));
		bolts.add(new Bolt(8));
		bolts.add(new Bolt(3));

		return bolts;
		
	}


	/**
     * Create bolts Map from an input bolts Set with each bolt size as key
     * 
     * @param bolts The input bolts Set
     * @return the output temporary bolts Map
     */
	private static Map<Integer, Bolt> createTemporaryBoltsMap(Set<Bolt> bolts) {

		Map<Integer, Bolt> tempBoltsMap = new HashMap<>();
		for (Bolt bolt: bolts) {
			tempBoltsMap.put(bolt.getSize(), bolt);
        }
		
		return tempBoltsMap;
	}


    /**
     * Creates the resulting match map with a relationship of one nut to one bolt
     * @param nuts The nuts input set
     * @param tempBoltsMap The temporary bolts map with their size as key
     * @return The resulting match map of nuts with bolts
     */
    private static MutableBiMap<Nut, Bolt> createResultMatchMap(Set<Nut> nuts, Map<Integer, Bolt> tempBoltsMap) {

		MutableBiMap<Nut, Bolt> resultMatchMap = HashBiMap.newMap(); 
															   		 
		Iterator<Nut> nutsIterator = nuts.iterator();
		while(nutsIterator.hasNext()) {
		   Nut nut = nutsIterator.next();
		   
		   resultMatchMap.put(nut, tempBoltsMap.get(nut.getSize()));

		}
		
		return resultMatchMap;

    	
	}



    /**
     * Iterate over the resulting match map
     * @param resultMatchMap The resulting nuts-bolts map
     */
	private static void iterateResultingMap(MutableBiMap<Nut, Bolt> resultMatchMap) {

		LOGGER.info("Now, lets just iterate over the resulting match map...");

		for (Map.Entry<Nut, Bolt> entry : resultMatchMap.entrySet()) {
			Nut nut = entry.getKey();
			Bolt bolt = entry.getValue();

			LOGGER.info("Nut size {} matching with Bolt size {}", nut.getSize(), bolt.getSize()); // prints the match of nut size with bolt size
		}
		
	}



}
