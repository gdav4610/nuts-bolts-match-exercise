# nuts-bolts-match-exercise


## Getting started

For this you will need to have installed:

- [ ] Java 8

- [ ] Maven 3.x

## To clone this repo use

```
git clone https://gitlab.com/gdav4610/nuts-bolts-match-exercise.git
```


## Name
Nuts and bolts Oracle exercise

## Description
This is the nuts and bolts exercise that was asked to complete for the Oracle interview.
